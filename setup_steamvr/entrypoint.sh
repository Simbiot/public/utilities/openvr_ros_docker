#!/bin/bash
# Note: I've written this using sh so it works in the busybox container too

# USE the trap if you need to also do manual cleanup after the service is stopped,
#     or need to start multiple services in the one container
trap "echo TRAPed signal" HUP INT QUIT TERM
set -e

./$STEAMVR_DIR/bin/vrmonitor.sh &> /dev/null &
echo "[hit enter key to start ros node]"
read

echo "starting ros service_pose"
source /opt/ros/melodic/setup.bash
source ~/catkin_ws/devel/setup.bash
roslaunch pyopenvr_wrapper_ros srv_and_tf.launch

echo "exited $0"
