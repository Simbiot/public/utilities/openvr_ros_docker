#!/bin/bash -x
TOOLSDIR=$(cd $(dirname $0)/..; pwd)
echo $TOOLSDIR
RUNTIMEDIR=$(cd $TOOLSDIR/../runtime; pwd)
echo $RUNTIMEDIR

SETUP_LOG=/tmp/SteamVRLauncherSetup.log

function pErr()
{
	echo "$(date) - $@" >> $SETUP_LOG
}

function SteamVRLauncherSetup()
{
	if ! [ -x "$(command -v getcap)" ]; then
		pErr 'Error: getcap is required to complete the SteamVR setup.'
		return 1
	fi

	if ! [ -x "$(command -v setcap)" ]; then
		pErr 'Error: setcap is required to complete the SteamVR setup.'
		return 1
	fi

	if [[ "$(getcap $TOOLSDIR/bin/linux64/vrcompositor-launcher)" == *"cap_sys_nice"* ]]; then
		return 0
	fi

	# if ! zenity --no-wrap --question --text="SteamVR requires superuser access to finish setup. Proceed?"; then
	# 	pErr 'Error: user declined superuser request.'
	# 	return 1
	# fi

	# pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY setcap CAP_SYS_NICE=eip $TOOLSDIR/bin/linux64/vrcompositor-launcher
	pkexec setcap CAP_SYS_NICE=eip $TOOLSDIR/bin/linux64/vrcompositor-launcher

	if ! [[ "$(getcap $TOOLSDIR/bin/linux64/vrcompositor-launcher)" == *"cap_sys_nice"* ]]; then
		pErr 'Error: setcap of vrcompositor-launcher failed.'
		return 1
	fi

	return 0
}

case $(uname) in
	Darwin)
		QT_DIR=$(cd $TOOLSDIR/../../src/do_not_distribute/qt/5.5.0/osx32; pwd)
		export QT_PLUGIN_PATH=$QT_DIR/plugins
		export DYLD_LIBRARY_PATH=$RUNTIMEDIR/bin:$RUNTIMEDIR/bin/osx32
		export DYLD_FRAMEWORK_PATH=$QT_DIR/lib
		VRSTARTUP=$TOOLSDIR/bin/osx32/vrstartup
		;;
	Linux)
		if [ -z "$STEAM_RUNTIME" ]; then
			exec ~/.steam/steam/ubuntu12_32/steam-runtime/run.sh $0 $*
		fi

		VRSTARTUP=$TOOLSDIR/bin/linux64/vrstartup
		QT_DIR=$TOOLSDIR/bin/linux64/qt
		export LD_LIBRARY_PATH=.:$RUNTIMEDIR/bin/linux64:$QT_DIR/lib:$TOOLSDIR/bin/linux64:$LD_LIBRARY_PATH
		export VRCOMPOSITOR_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"

		SteamVRLauncherSetup
		# if [ "$?" != "0" ]; then
		# 	zenity --no-wrap --info --text="SteamVR setup is incomplete, some features might be missing. See $SETUP_LOG for details.";
		# fi
		;;
	default)
		echo set QT_DIR
		exit 1
		;;
esac

exec $DEBUGGER $VRSTARTUP $*
